# SpringCloudConfigClient

Sample Spring Clould Config Client

## Description

Sample how to use Spring Cloud Config Client with a Server (check SpringCloudConfigServer)
Read configuration from Spring Cloud Config Server and use it

## Purpose
Spread configurations across microservices using application.yml properties
The configuration is Server/Client. Server will provide configuration values from a key-value table and the client will use the server as configuration source.

## Usage

just run the app without having a server and with a server and in the log you will see the difference

without a running server - the configuration will be
-- SpringCloudConfigConfiguration(configA=from Java A, configB=from Java B)
and with a running server
-- SpringCloudConfigConfiguration(configA=Config A, configB=Config B)

## Caveats
Configuration spring.config.import: optional:configserver:http://127.0.0.1:8080/cloud
should point to the server endpoint


## General

Official documentation Spring Cloud Config - https://docs.spring.io/spring-cloud-config/docs/current/reference/html/

update pom

        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-bus-amqp</artifactId>
            <version>2.2.1.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-config</artifactId>
        </dependency>
update application yml


config:

    import: optional:configserver:<path to your Spring Cloud config server including prefix>

    cloud:
        config:
            enabled: true
            name: app
            profile: pro
            label: master
The name, profile, label are optional - if you do not specify them the default will be used.

Define a configuration

    @Configuration
    @ComponentScan
    public class SpringCloudConfigServerApplicationConfiguration {

        @Bean
        public JdbcEnvironmentRepository jdbcEnvironmentRepository(JdbcEnvironmentRepositoryFactory factory,
            JdbcEnvironmentProperties environmentProperties) {
            return factory.build(environmentProperties);
        }
    }
or use the configuration direct

    @Value("${configA}")
    private String configA = "x";
run and check values - you should receive values from Spring Cloud Config Server


What you should see in the LOG of the client

with running server

    2022-07-19 14:09:05.480  INFO 4708 --- [           main] o.s.c.c.c.ConfigServerConfigDataLoader   : Fetching config from server at : http://127.0.0.1:8080/cloud
    2022-07-19 14:09:05.480  INFO 4708 --- [           main] o.s.c.c.c.ConfigServerConfigDataLoader   : Located environment: name=app, profiles=[pro], label=master, version=null, state=null
and the variables should be initialized with values from Spring Cloud Config server instance

and without running server
    
    2022-07-19 14:24:06.342  INFO 3000 --- [           main] o.s.c.c.c.ConfigServerConfigDataLoader   : Fetching config from server at : http://127.0.0.1:8080/cloud
    2022-07-19 14:24:06.342  INFO 3000 --- [           main] o.s.c.c.c.ConfigServerConfigDataLoader   : Connect Timeout Exception on Url - http://127.0.0.1:8080/cloud. Will be trying the next url if available
and the variables should be initialized with default values
    
## License
GPL
