package ro.fagadar.springcloudconfigclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringcloudconfigclientApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext configurableApplicationContext = SpringApplication
            .run(SpringcloudconfigclientApplication.class, args);
        configurableApplicationContext.getBean(MainClass.class).init();
    }
}
