package ro.fagadar.springcloudconfigclient;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties
@Data
public class SpringCloudConfigConfiguration {
    private String configA = "from Java A";

    private String configB = "from Java B";
}