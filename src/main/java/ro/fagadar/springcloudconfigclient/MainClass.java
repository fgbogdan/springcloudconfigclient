package ro.fagadar.springcloudconfigclient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MainClass {

    @Autowired
    SpringCloudConfigConfiguration springCloudConfigConfiguration;

    public void init() {
        log.info("Values from Spring Cloud Config Server ...");
        log.info(springCloudConfigConfiguration.toString());
    }
}
